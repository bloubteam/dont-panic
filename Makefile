# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tbazire <tbazire@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/01/22 10:00:44 by tbazire           #+#    #+#              #
#    Updated: 2014/08/06 23:08:32 by lcaminon         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		=	DontPanic

CC			=	g++
CFLAGS		=	-Wall -Werror -Wextra -O3 -std=c++11	\
				-I $(PWD)/include
LFLAGS		=	-lBox2D -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio

FILESRC		=	Batitest.cpp \
				Brick.cpp \
				Button.cpp \
				Cone.cpp \
				Control.cpp \
				Editor.cpp \
				Game.cpp \
				ListButon.cpp \
				main.cpp \
				Menu.cpp \
				Player.cpp \
				RayCastVision.cpp \
				Ressources.cpp \
				SoftCircle.cpp

DIRSRC		=	src
DIROBJ		=	.obj

SRC			=	$(addprefix $(DIRSRC)/, $(FILESRC))
OBJ			=	$(addprefix $(DIROBJ)/, $(SRC:.cpp=.o))

all	:	$(NAME)

$(addprefix $(DIROBJ)/, %.o)	:	%.cpp
	$(CC) $(CFLAGS) -o $@ -c $<
	printf '\033[2K\033[1;34mCreate object [\033[0m%s\033[1;34m]\033[0m\n\
\033[1A' "$<"

$(NAME)	:	$(DIROBJ) $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) $(LFLAGS)
	printf '\033[2K'
	printf '%s \033[32m√\033[0m\n' "$@"

$(DIROBJ)	:
	/bin/mkdir	$@
	/bin/mkdir	$@/$(DIRSRC)

clean	:
	/bin/rm -rf $(DIROBJ)
	printf '\033[1;30m# Remove %s\033[0m\n' "$(DIROBJ)"

fclean	:	clean
	/bin/rm -f $(NAME)
	printf '\033[1;30m# Remove %s\033[0m\n' "$(NAME)"

debug	:	CFLAGS += -g -O
debug	:	re

# Change "-W" to "-Weverything" with bash
every	:	CFLAGS += -W -O3
every	:	re

re		:	fclean all

$(LIB)	:
	make -C $@/

lfclean	:
	make -C $@ fclean

.PHONY: all clean fclean re

.SILENT:
