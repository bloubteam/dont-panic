// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   sprite.hh                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cplumas <marvin@42.fr>                     +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/22 20:58:15 by cplumas           #+#    #+#             //
//   Updated: 2014/06/23 12:47:16 by cplumas          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef		SPRITE_HH
# define	SPRITE_HH

#include                <SFML/Window.hpp>
#include                <SFML/Graphics.hpp>
#include                <SFML/System.hpp>
#include                <iostream>
#include                <vector>

enum Dir {S, A, D, W};

class		Sprite
{
public:
	Sprite();
	int		CreateSprite(std::string name);
	void	Keyboard();
	void	NextSprite();

private:
	int		speed;
};

#endif
