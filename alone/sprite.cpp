// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   youtube.cpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cplumas <marvin@42.fr>                     +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/22 12:36:45 by cplumas           #+#    #+#             //
//   Updated: 2014/06/23 12:49:11 by cplumas          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include				"sprite.hh"

sf::Texture perso;
sf::Sprite sprite_perso;
sf::Vector2i anim(1, S);

Sprite::Sprite()
{
}

int		Sprite::CreateSprite(std::string name)
{

	if (!perso.loadFromFile(name))
		return (0);
	perso.setSmooth(true);
	sprite_perso.setTexture(perso);
}
void	Sprite::NextSprite()
{
	if (anim.x * 32 >= perso.getSize().x)
		anim.x = 0;
	sprite_perso.setTextureRect(sf::IntRect(anim.x * 32, anim.y * 32, 32, 32));
}

void	Sprite::Keyboard()
{
	speed = 2;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		anim.y = W;	
		sprite_perso.move(0, -speed);
		anim.x++;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		anim.y = S;	
		sprite_perso.move(0, speed);
		anim.x++;	
	}	
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		anim.y = A;			
		sprite_perso.move(-speed, 0);
		anim.x++;	
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		anim.y = D;	
		sprite_perso.move(speed, 0);
		anim.x++;	
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		exit(0);
}

// int		main()
// {
// 	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML");
// 	sf::Event event;
// 	Sprite	my_sprite;

// 	window.setMouseCursorVisible(false);
// 	my_sprite.CreateSprite("image.png");
// 	while(window.isOpen())
// 	{
// 		while (window.pollEvent(event))
// 		{
// 			if (event.type == sf::Event::Closed)
// 				window.close();
// 		}
// 		my_sprite.Keyboard();
// 		my_sprite.NextSprite();
// 		window.clear();
// 		window.draw(sprite_perso);
// 		window.display();
// 	}
// 	return (0);
// }
