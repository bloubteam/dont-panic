// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   souris.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cplumas <marvin@42.fr>                     +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/22 02:22:53 by cplumas           #+#    #+#             //
//   Updated: 2014/06/22 02:33:44 by cplumas          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <SFML/Graphics.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <iostream>


int			main()
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "Hidden Cursor");
    window.setMouseCursorVisible(false);
	sf::View fixed = window.getView();
	sf::Texture texture;
    texture.loadFromFile("target.png");
	sf::Sprite sprite(texture);
	sf::CircleShape	shape(20);
	sf::Event event;
	sf::RectangleShape bord;
	sf::RectangleShape life;
    float x;

	x = 0;
	shape.setFillColor(sf::Color(9, 106, 9));
	shape.setPosition(400, 300);
	bord.setFillColor(sf::Color::Transparent);
    bord.setOutlineThickness(5);
    bord.setOutlineColor(sf::Color(250, 250, 250));
    bord.setSize(sf::Vector2f(200, 20));
    bord.setPosition(550, 550);
    life.setFillColor(sf::Color(250, 0, 0));
    life.setSize(sf::Vector2f(x, 20));
    life.setPosition(550, 550);   
	while (window.isOpen())
    {
        life.setSize(sf::Vector2f(x, 20));
        if (x < 200)
            x = x + 0.05;
        while (window.pollEvent(event))
        {
            switch (event.type)
            {
            case sf::Event::KeyPressed:
            {
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
                    shape.move(0, -5);
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
                    shape.move(0, 5);
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
                    shape.move(5, 0);
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
                    shape.move(-5, 0);
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
                    window.close();
            }
            case sf::Event::MouseMoved:
            {
				std::cout << "mouse x: " << event.mouseMove.x << std::endl;
				std::cout << "mouse y: " << event.mouseMove.y << std::endl;
            }
            case sf::Event::MouseButtonPressed:
			{
                if (event.mouseButton.button == sf::Mouse::Right)
					std::cout << "Item Used" << std::endl;
                else if (event.mouseButton.button == sf::Mouse::Left)
					std::cout << "Open Fire Bitch !" << std::endl;
            }
            default:
                break;
            }
			sprite.setPosition(static_cast<sf::Vector2f>
							   (sf::Mouse::getPosition(window)));
			window.clear();
			window.setView(fixed);
			window.draw(shape);
			window.draw(sprite);
			window.draw(bord);
			window.draw(life);
			window.display();
		}
	}
	return (0);
}
