// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Brick.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/07/07 17:08:13 by lcaminon          #+#    #+#             //
//   Updated: 2014/07/12 15:29:34 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Brick.h>

// R G B A
// R => forme/collision
// G => lumière
// B A => texture

std::shared_ptr<IElement> Brick::make(b2World &world, sf::Color type, float posx, float posy)
{
	b2BodyDef bodyDef;
	bodyDef.position = b2Vec2(posx, posy);
	bodyDef.type = b2_staticBody;
	b2Body* body = world.CreateBody(&bodyDef);
     
	b2PolygonShape shape;
	shape.SetAsBox((Brick::brickSize/2)/SCALE, (Brick::brickSize/2)/SCALE);
	b2FixtureDef fixtureDef;
	fixtureDef.density = 1.f;
	fixtureDef.friction = 0.7f;
	fixtureDef.shape = &shape;
	fixtureDef.filter.categoryBits = COL_PHYSIC;
	if (type == sf::Color::Blue)
		fixtureDef.filter.maskBits = COL_PHYSIC;
	else
		fixtureDef.filter.maskBits = COL_LIGHT | COL_VISION | COL_PHYSIC;
	body->CreateFixture(&fixtureDef);
	std::shared_ptr<Brick> brick = std::make_shared<Brick>(body, type);
	body->SetUserData(brick.get());
	brick->setPosition(posx, posy);

	return (brick);
}

Brick::Brick(b2Body *oself, sf::Color type)
{
	self = oself;
	_brick.setSize(sf::Vector2f(Brick::brickSize/SCALE, Brick::brickSize/SCALE));
	_brick.setFillColor(type);
	_brick.setOutlineThickness(0);
	_brick.setOrigin(_brick.getSize().x / 2, _brick.getSize().y / 2);
}

bool Brick::dead()
{
	return (false);
}

void Brick::remove(b2World& world)
{
	(void)world;
}

void Brick::update(sf::RenderTexture &luminosity, b2World &world, float timeStep)
{
	(void)luminosity;
	(void)world;
	(void)timeStep;
}

void Brick::luminosity(sf::RenderTexture &luminosity, b2World &world)
{
	(void)luminosity;
	(void)world;
}

void Brick::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(_brick, states);
}
