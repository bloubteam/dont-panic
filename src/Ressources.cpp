//
// Ressources.cpp for similis in /home/lcaminon/Documents/projets/similis
//
// Made by Loic Caminondo
// Email   <loic@caminondo.fr>
//
// Started on  Tue Sep 17 15:08:04 2013 Loic Caminondo
// Last update Sun Nov 17 02:54:38 2013 Loic Caminondo
//

#include		"Ressources.hh"

Ressources::Ressources()
  : _th_loading(&Ressources::loading, this)
{
  _isLoading = false;
  _loadingError = false;
}

Ressources::~Ressources()
{
}

void			Ressources::loading()
{
  _texts[LAN_FR][TEX_NEW] = "Nouvelle Partie";
  _texts[LAN_FR][TEX_CONTINUE] = "Continuer";
  _texts[LAN_FR][TEX_EDITOR] = "Outil d'edition";
  _texts[LAN_FR][TEX_AGAIN] = "Recommencer";
  _texts[LAN_FR][TEX_OPTIONS] = "Options";
  _texts[LAN_FR][TEX_AUDIO] = "Audio";
  _texts[LAN_FR][TEX_GRAPHICS] = "Graphismes";
  _texts[LAN_FR][TEX_CONTROLS] = "Controles";
  _texts[LAN_FR][TEX_RETURN] = "Retour";
  _texts[LAN_FR][TEX_SUCCESS] = "Haut Fait";
  _texts[LAN_FR][TEX_ANALYSE] = "Statistiques";
  _texts[LAN_FR][TEX_EXIT] = "Quitter";
  _texts[LAN_FR][TEX_STATS] = "Caracteristiques";
  _texts[LAN_FR][TEX_SKILLS] = "Competences";
  _texts[LAN_FR][TEX_YES] = "Oui";
  _texts[LAN_FR][TEX_NO] = "Non";
  _texts[LAN_FR][TEX_AREYOUSURE] = "Etes-vous sur ?";
  _texts[LAN_FR][TEX_COMINGSOON] = "A venir ...";
  _language = LAN_FR;
  if (!_font.loadFromFile("resources/ZOMBIFIED.ttf"))
    _loadingError = true;
  _isLoading = false;
  _statut = STA_MENU;
}

void			Ressources::load(sf:: RenderWindow *window)
{
  _isLoading = true;
  _window = window;
  _th_loading.launch();
}

bool			Ressources::isLoading()
{
  return (_isLoading);
}

bool			Ressources::isOk()
{
  return (!_loadingError);
}

sf::Font		*Ressources::getFont(FontType type)
{
  (void)type;
  return (&_font);
}

sf::Texture		*Ressources::getTexture(TextureType type)
{
  (void)type;
  return (NULL);
}

sf::RenderWindow	*Ressources::getWindow()
{
  return (_window);
}
/*
sf::RenderWindow	*Ressources::getAudio()
{
  return (_audio);
}
*/
std::string		Ressources::getText(Text text)
{
    return (_texts[_language][text]);
}

int			Ressources::getDef()
{
  if (_quality == QUA_HIG)
    return (360);
  else if (_quality == QUA_MED)
    return (72);
  else
    return (12);
}

State             Ressources::getStatut()
{
    return (this->_statut);
}

void            Ressources::setStatut(State statut)
{
    this->_statut = statut;
}
