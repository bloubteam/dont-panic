// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   SoftCircle.cpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/21 17:29:04 by lcaminon          #+#    #+#             //
//   Updated: 2014/06/22 16:56:42 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <SoftCircle.h>

SoftCircle::SoftCircle(sf::Color in, sf::Color out, float plainRadius, float degradeSize)
  : _inside(sf::TrianglesFan),
    _outside(sf::TrianglesStrip)
{
	_in = in;
	_out = out;
	_plainRadius = plainRadius;
	_degradeSize = degradeSize;
	calcPos();
}

void		SoftCircle::calcPos()
{
	sf::Transform rotate;
	int		i;

	int def = 100;
	_inside.resize(def + 1);
	_outside.resize(def * 2);
	_inside[0].position = sf::Vector2<float>(0, 0);
	_inside[0].color = _in;
	i = -1;
	while (++i < def)
    {
		rotate.rotate(360. / def + 1, 0, 0);
		_inside[i + 1].position = rotate.transformPoint(_plainRadius, 0);
		_inside[i + 1].color = _in;
		_outside[i * 2].position = rotate.transformPoint(_plainRadius + _degradeSize, 0);
		_outside[i * 2].color = _out;
		_outside[i * 2 + 1].position = rotate.transformPoint(_plainRadius, 0);
		_outside[i * 2 + 1].color = _in;
    }
}

void SoftCircle::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(_inside, states);
	target.draw(_outside, states);
}
