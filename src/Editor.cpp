// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Game.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/20 22:12:39 by lcaminon          #+#    #+#             //
//   Updated: 2014/08/11 02:47:27 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Editor.h>

Editor::Editor()
	: _gravity(0, 0),
	  _world(_gravity)
{
	_window = Ressources::getInstance()->getWindow();
	_view = _window->getDefaultView();
	_view.zoom(1/SCALE);
	_view.setCenter(0, 0);
	if (!_light.create(_view.getSize().x * SCALE, _view.getSize().y * SCALE))
		throw ;
}

Editor::~Editor()
{
}

void Editor::event()
{
	sf::Event event;

	while (_window->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			_window->close();
		else if (event.type == sf::Event::KeyPressed)
		{
		  if (event.key.code == sf::Keyboard::Escape)
		      _window->close();
		  if (event.key.code == sf::Keyboard::Left)
			  _view.move(-1, 0);
		  if (event.key.code == sf::Keyboard::Right)
			  _view.move(1, 0);
		  if (event.key.code == sf::Keyboard::Up)
			  _view.move(0, -1);
		  if (event.key.code == sf::Keyboard::Down)
			  _view.move(0, 1);
		}
		else if (event.type == sf::Event::MouseButtonPressed)
		{
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				sf::Vector2f pos = _window->mapPixelToCoords(sf::Mouse::getPosition(*_window));
				if (_control.event(pos))
					continue;
			}
			if (event.mouseButton.button == sf::Mouse::Right)
			{
			}
		}
	}
}

void Editor::luminosity()
{
	sf::RenderStates render(sf::BlendMode::BlendAdd);
	_light.clear(_control.getAmbiant()); // luminosité ambiante

	_light.display();
}

void Editor::draw()
{
	_window->clear(sf::Color::White);
	//
	// dessiner tout ce qui est influencé par la lumière
	//
	luminosity();
	sf::View		view;
	view = _window->getView();
	sf::RenderStates render(sf::BlendMode::BlendMultiply);
	sf::Sprite spriteLum(_light.getTexture());
	spriteLum.scale(1/SCALE, 1/SCALE);
	spriteLum.setPosition(view.getCenter().x - view.getSize().x / 2, view.getCenter().y - view.getSize().y / 2);
	_window->draw(spriteLum, render);
	//
	//dessiner tout ce qui va au dessus des lumières (sans render)
	//
	_control.setPosition(view.getCenter().x - view.getSize().x / 2, view.getCenter().y - view.getSize().y / 2);
	_control.setSize(sf::Vector2f(view.getSize().x / 3., view.getSize().y));
	_window->draw(_control);
	_window->display();
}

void Editor::update()
{
	_control.update();
}

void Editor::main()
{
	sf::Clock clock;
	sf::Time timeStep;

	_window->setMouseCursorVisible(true);
	while (_window->isOpen())
	{
		timeStep = clock.restart();
		event();
		update();
		_window->setView(_view);
		draw();
	}
}
