// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ListButon.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/07/20 22:22:30 by lcaminon          #+#    #+#             //
//   Updated: 2014/08/10 00:54:53 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <ListButon.h>

ListButon::ListButon(std::list<std::string> &list)
	: _left(20 / SCALE, 3),
	  _right(20 / SCALE, 3),
	  _list(list)
{
    Ressources          *ressources = Ressources::getInstance();

    _text.setFont(*ressources->getFont());
    _text.setCharacterSize(40);
	_text.scale(1/SCALE, 1/SCALE);
	_text.setColor(sf::Color::Red);
	_left.setOrigin(20/SCALE, 20/SCALE);
	_left.rotate(30);
	_left.setFillColor(sf::Color::Red);
	_right.setOrigin(20/SCALE, 20/SCALE);
	_right.rotate(-30);
	_right.setPosition(80/SCALE, 0);
	_right.setFillColor(sf::Color::Red);
}

void ListButon::update()
{
	if (!_list.empty())
		_text.setString(*_list.begin());
	_text.setOrigin(_text.getLocalBounds().width / 2, _text.getLocalBounds().height / 2);
	_left.setPosition(-_text.getLocalBounds().width / 2 / SCALE - _left.getRadius(), 0);
	_right.setPosition(_text.getLocalBounds().width / 2 / SCALE + _right.getRadius(), 0);
}

void ListButon::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(_left, states);
	target.draw(_right, states);
	target.draw(_text, states);
}

bool ListButon::event(sf::Vector2f pos)
{
	if (_list.size() > 1)
	{
		pos = getTransform().getInverse().transformPoint(pos);
		if (_left.getGlobalBounds().contains(pos))
		{
			std::rotate(_list.rbegin(), std::next(_list.rbegin(), 1), _list.rend());
			return (true);
		}
		if (_right.getGlobalBounds().contains(pos))
		{
			std::rotate(_list.begin(), std::next(_list.begin(), 1), _list.end());
			return (true);
		}
	}
	return (false);
}
