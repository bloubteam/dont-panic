// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Game.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/20 22:12:39 by lcaminon          #+#    #+#             //
//   Updated: 2014/08/02 04:09:29 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Game.h>

Game::Game()
	: _gravity(0, 0),
	  _world(_gravity)
{
	_window = Ressources::getInstance()->getWindow();
	_view = _window->getDefaultView();
	_view.zoom(1/SCALE);
	_player = Player::make(_world);
	_elements.push_front(_player);
	_elements.push_front(Batitest::make(_world));
	if (!_scene.create(_view.getSize().x * SCALE, _view.getSize().y * SCALE))
		throw ;
	if (!_light.create(_view.getSize().x * SCALE, _view.getSize().y * SCALE))
		throw ;
	if (!_vision.create(_view.getSize().x * SCALE, _view.getSize().y * SCALE))
		throw ;
	if (!_visionShader.loadFromFile("resources/visionShader.frag", sf::Shader::Fragment)) // caller ca dans ressources
		throw ;
	_visionShader.setParameter("texture", sf::Shader::CurrentTexture);
	_visionShader.setParameter("size", static_cast<sf::Vector2f>(_window->getSize()));
}

Game::~Game()
{
}

void Game::event()
{
	sf::Event event;

	while (_window->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			_window->close();
		else if (event.type == sf::Event::KeyPressed)
		{
		  if (event.key.code == sf::Keyboard::Escape)
		    {
		      _window->close();
		    }
		}
		else if (event.type == sf::Event::MouseButtonPressed)
		{
			if (event.mouseButton.button == sf::Mouse::Left)
			{
			}
			if (event.mouseButton.button == sf::Mouse::Right)
			{
			}
		}
	}
}

void Game::prepareToMove()
{
	// réveiller endormir les entités (économies de calculs)
}

void Game::luminosity()
{
	sf::RenderStates render(sf::BlendMode::BlendAdd);
	//_light.clear(sf::Color(45, 45, 10)); // luminosité ambiante
	_light.clear(sf::Color::White); // luminosité ambiante

	Cone cone(sf::Color(200, 200, 100), sf::Color::Black, 300);
	cone.update(_player->self->GetPosition(), _player->self->GetAngle(), 80, 400, _world);
	Cone coneb(sf::Color(200, 0, 100), sf::Color::Black, 300);
	coneb.update(b2Vec2(160/SCALE, 2000/SCALE), 1.6, 80, 400, _world);
	Cone conec(sf::Color(0, 200, 100), sf::Color::Black, 300);
	conec.update(b2Vec2(200/SCALE, 2000/SCALE), 1.8, 80, 400, _world);
	SoftCircle circlea(sf::Color(255, 255, 255, 255), sf::Color(255, 255, 255, 0), 10/SCALE, 100/SCALE);
	circlea.move(100/SCALE, 100/SCALE);
	_light.draw(circlea, render);
	//_light.draw(cone, render);
	_light.draw(coneb, render);
	_light.draw(conec, render);
	_light.display();
}

void Game::vision()
{
	_vision.clear(sf::Color::Black);
	_player->vision(_vision, _world);
	_vision.display();
}

void Game::draw()
{
	sf::View		view;
	view = _window->getView();
	sf::RenderStates render(sf::BlendMode::BlendMultiply);

	sf::Sprite spriteLum(_light.getTexture());
	spriteLum.scale(1/SCALE, 1/SCALE);
	spriteLum.setPosition(view.getCenter().x - view.getSize().x / 2, view.getCenter().y - view.getSize().y / 2);
	sf::Sprite spriteVis(_vision.getTexture());
	spriteVis.scale(1/SCALE, 1/SCALE);
	spriteVis.setPosition(view.getCenter().x - view.getSize().x / 2, view.getCenter().y - view.getSize().y / 2);
	_scene.draw(spriteLum, render);
	_scene.draw(spriteVis, render);
	_scene.display();
}

void Game::main()
{
	sf::Clock clock;
	sf::Time timeStep;

	timeStep = clock.restart();
	_window->setMouseCursorVisible(false);
	while (_window->isOpen())
	{
		timeStep = clock.restart();
		prepareToMove();
		event();
		_player->move(*_window);
		_world.Step(timeStep.asSeconds(), 8, 3);
		b2Vec2 vect = _player->self->GetPosition();
		_view.setCenter(vect.x, vect.y);
		_window->setView(_view);
		_scene.setView(_view);
		_light.setView(_view);
		_vision.setView(_view);
        _visionShader.setParameter("view", sf::Vector2f(sf::Mouse::getPosition(*_window).x, _window->getSize().y - sf::Mouse::getPosition(*_window).y));
		_scene.clear(sf::Color::White);

		// center la fenetre sur le personnage
		// dessiner le sol qui va bien
		for (b2Body* bodyIterator = _world.GetBodyList(); bodyIterator != 0;)
		{
			b2Body *actBody = bodyIterator;
			bodyIterator = bodyIterator->GetNext();
			IElement *element = reinterpret_cast<IElement*>(actBody->GetUserData());

			// detruire s'qui faut détruire et dessiner l'bousin
			if (element->dead())
			{
				// delete element (de façon memory-friendly => sharp-pointer négro !)
				element->remove(_world);
			}
			else
			{
				element->update(_light, _world, timeStep.asSeconds());
				_scene.draw(*element);
			}
		}
		
		luminosity();
		vision();
		draw();
		_window->clear(sf::Color::White);
		sf::RenderStates render;
		render.shader = &_visionShader;
		sf::Sprite spriteSce(_scene.getTexture());
		spriteSce.scale(1/SCALE, 1/SCALE);
		spriteSce.setPosition(_view.getCenter().x - _view.getSize().x / 2, _view.getCenter().y - _view.getSize().y / 2);
		_window->draw(spriteSce, render);
		_window->display();
	}
}
