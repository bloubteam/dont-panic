// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Player.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/21 01:42:51 by lcaminon          #+#    #+#             //
//   Updated: 2014/07/08 07:09:13 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Player.h>

std::shared_ptr<Player> Player::make(b2World &world)
{
	b2BodyDef bodyDef;
	bodyDef.position = b2Vec2(0, 0);
	bodyDef.type = b2_dynamicBody;
	b2Body* body = world.CreateBody(&bodyDef);
     
	b2PolygonShape shape;
	shape.SetAsBox((65.f/2)/SCALE, (50.f/2)/SCALE);
	b2FixtureDef fixtureDef;
	fixtureDef.density = 1.f;
	fixtureDef.friction = 0.7f;
	fixtureDef.shape = &shape;
	fixtureDef.filter.categoryBits = COL_PHYSIC;
	fixtureDef.filter.maskBits = COL_PHYSIC;
	body->CreateFixture(&fixtureDef);
	std::shared_ptr<Player> player = std::make_shared<Player>(body);
	body->SetUserData(player.get());

	return (player);
}

Player::Player(b2Body *oself)
	: _coneVision(sf::Color::White, sf::Color::Black, 600),
	  _fishEye(180),
	  _distVision(940)
{
	self = oself;
	_body.setSize(sf::Vector2f(65/SCALE, 50/SCALE));
	_body.setFillColor(sf::Color::Red);
	_body.setOutlineThickness(0);
	_body.setOrigin(_body.getSize().x / 2, _body.getSize().y / 2);

	_leftArm.setSize(sf::Vector2f(20/SCALE, 20/SCALE));
	_leftArm.move(-22/SCALE, 5/SCALE);
	_leftArm.setFillColor(sf::Color::Red);
	_leftArm.setOutlineThickness(0);
	_leftArm.setOrigin(_body.getSize().x / 2, _body.getSize().y / 2 - _leftArm.getSize().y);
}

bool Player::dead()
{
	return (false);
}

void Player::vision(sf::RenderTexture &vision, b2World &world)
{
	_coneVision.update(self->GetPosition(), self->GetAngle(), _fishEye, _distVision, world);
	vision.draw(_coneVision);
}

void Player::update(sf::RenderTexture &luminosity, b2World &world, float timeStep)
{
	(void)luminosity;
	(void)world;
	if (_running)
	{
		if (_fishEye > 120)
			_fishEye -= 480*timeStep;
		else
			_fishEye = 120;
	}
	else
	{
		if (_fishEye < 200)
			_fishEye += 180*timeStep;
		else
			_fishEye = 200;
	}
	b2Vec2 vect = self->GetPosition();
	setPosition(vect.x, vect.y);
	setRotation(self->GetAngle() * RADTODEG);
}

void Player::luminosity(sf::RenderTexture &luminosity, b2World &world)
{
	(void)luminosity;
	(void)world;
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(_body, states);
	target.draw(_leftArm, states);
}

void Player::move(sf::RenderWindow &window)
{
	sf::Vector2f mouse = window.mapPixelToCoords(sf::Mouse::getPosition(window));
	b2Vec2 target(mouse.x, mouse.y);
    b2Vec2 toTarget = target - self->GetPosition();
    float desiredAngle = atan2f(-toTarget.x, toTarget.y);
	self->SetAngularVelocity(0);
	self->SetTransform(self->GetPosition(), desiredAngle);

	b2Vec2 vect(0, 0);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)
		|| sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
	{
		vect.x -= 1;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)
		|| sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		vect.x += 1;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
		|| sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
	{
		vect.y -= 1;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
		|| sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		vect.y += 1;
	}
	float speedMax = 150/SCALE;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)
		|| sf::Keyboard::isKeyPressed(sf::Keyboard::RShift))
	{
		_running = true;
		speedMax *= 2;
	}
	else
		_running = false;
	float u = vect.Normalize();
	b2Vec2 currentVect = self->GetLinearVelocity();
	float currentSpeed = currentVect.Normalize();
	if (u <= 0.1 || currentSpeed > speedMax)
	{
		if (currentSpeed <= 0.1)
		{
			self->SetAwake(false);
			return ;
		}
		else
			vect = -currentVect;
	}
	vect *= speedMax * 20;
	self->ApplyForce(vect, self->GetWorldCenter(), true);
}

void Player::remove(b2World &world)
{
	(void)world;
}
