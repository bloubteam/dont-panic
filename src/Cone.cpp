// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Cone.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/22 16:33:16 by lcaminon          #+#    #+#             //
//   Updated: 2014/06/22 17:55:13 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Cone.h>

Cone::Cone(sf::Color in, sf::Color out, int def)
	: _visionArray(sf::TrianglesFan, def), _in(in), _out(out), _def(def)
{
}

void Cone::update(b2Vec2 origin, float dirAngle, float opAngle, float dist, b2World &world)
{
	dist /= SCALE;
	_visionArray[0].position = sf::Vector2f(origin.x, origin.y);
	_visionArray[0].color = _in;
	float currentAngle = (-(dirAngle * RADTODEG) - opAngle / 2) * DEGTORAD;
	for (int i = 1; i < _def; ++i)
	{
		RayCastVision raycast;
		b2Vec2 vect(sinf(currentAngle), cosf(currentAngle));
		b2Vec2 pb = origin + dist * vect;
		world.RayCast(&raycast, origin, pb);
		float fract = raycast.getFraction();
		vect = origin + ((dist * fract + 5/SCALE) * vect);
		_visionArray[i].position = sf::Vector2f(vect.x, vect.y);
		sf::Color out;
		out = _out * sf::Color(255*fract,255*fract,255*fract);
		out += _in * sf::Color(255*(1 - fract), 255*(1 - fract), 255*(1 - fract));
		_visionArray[i].color = out;
		currentAngle += (opAngle / (_def - 2)) * DEGTORAD;
	}
}

void Cone::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(_visionArray, states);
}
