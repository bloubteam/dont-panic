// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   RayCastVision.cpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/22 14:27:10 by lcaminon          #+#    #+#             //
//   Updated: 2014/06/23 09:51:39 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <RayCastVision.h>

RayCastVision::RayCastVision()
{
	_fraction = 1;
}

float32 RayCastVision::ReportFixture(b2Fixture* fixture, const b2Vec2 &point,
						  const b2Vec2& normal, float32 fraction)
{
	(void)fixture;
	(void)point;
	(void)normal;
	const b2Filter& filter = fixture->GetFilterData();
	if (!(filter.maskBits & (COL_LIGHT | COL_VISION)))
		return (1);
	_fraction = fraction;
	return (fraction);
}

float RayCastVision::getFraction()
{
	return (_fraction);
}
