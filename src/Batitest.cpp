// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Batitest.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/07/02 23:35:11 by lcaminon          #+#    #+#             //
//   Updated: 2014/08/11 00:38:01 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Batitest.h>

Batitest::Batitest(b2World &world)
{
	(void)world;
	//parser le shema et remplir avec ce qui va bien
	sf::Image image;

	image.loadFromFile("resources/batitest.png");
	sf::Vector2u size = image.getSize();
	for (unsigned int x = 0; x < size.x; ++x)
		for (unsigned int y = 0; y < size.y; ++y)
		{
			sf::Color type = image.getPixel(x, y);
			if (type != sf::Color::White)
				elements.push_back(Brick::make(world, type, x * Brick::brickSize / SCALE, y * Brick::brickSize / SCALE));
		}
}

bool Batitest::dead()
{
	return (false);
}

void Batitest::remove(b2World& world)
{
	(void)world;
	//détruire tout les éléments de world
}

void Batitest::update(sf::RenderTexture &luminosity, b2World &world, float timeStep)
{
	(void)luminosity;
	(void)world;
	(void)timeStep;
}

void Batitest::luminosity(sf::RenderTexture &luminosity, b2World &world)
{
	(void)luminosity;
	(void)world;
}

void Batitest::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	for (auto it = elements.begin(); it != elements.end(); ++it)
		target.draw(**it, states);
}

std::shared_ptr<IElement> Batitest::make(b2World &world)
{
	std::shared_ptr<Batitest> batitest = std::make_shared<Batitest>(world);

	return (batitest);
}
