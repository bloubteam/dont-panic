// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/20 22:09:11 by lcaminon          #+#    #+#             //
//   Updated: 2014/07/21 17:57:40 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Menu.h>
#include <Game.h>
#include <Editor.h>

int main()
{
    Ressources   *ressources = Ressources::getInstance();

	try
	{
		std::cout << "Box2D v" << b2_version.major << "." << b2_version.minor << "." << b2_version.revision << std::endl;
        sf::RenderWindow window(*(sf::VideoMode::getFullscreenModes().begin()), "DontPanic");//sf::Style::Fullscreen);
		window.setFramerateLimit(60);
        ressources->load(&window); // fichier sauvegarde
		Game    game;
		Menu    menu;
		Editor	editor;

        while (ressources->isLoading());
        if (!ressources->isOk())
        {
            throw;
        }
        while (window.isOpen())
        {
            if (ressources->getStatut() == STA_MENU)
                menu.main();
            else if (ressources->getStatut() == STA_GAME)
                game.main();
			else if (ressources->getStatut() == STA_EDITOR)
				editor.main();
        }
        ressources->kill();
        return (0);
    }
	catch (...)
	{
		std::cout << "Sorry, an unhandled exeption occured :(" << std::endl;
        ressources->kill();
		return (-1);
	}
}
