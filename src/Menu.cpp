#include    <Menu.h>

Menu::Menu() :   _buttons {{
		{MEN_MAIN, {{Ressources::getInstance()->getText(TEX_NEW), &Menu::doLaunch},
				{Ressources::getInstance()->getText(TEX_CONTINUE), &Menu::doResume},
				{Ressources::getInstance()->getText(TEX_EDITOR), &Menu::doEditor},
				{Ressources::getInstance()->getText(TEX_OPTIONS), &Menu::doOptions},
				{Ressources::getInstance()->getText(TEX_EXIT), &Menu::doExit}}},
		{MEN_OPT, {{Ressources::getInstance()->getText(TEX_AUDIO), &Menu::doAudio},
				{Ressources::getInstance()->getText(TEX_GRAPHICS), &Menu::doGraphics},
				{Ressources::getInstance()->getText(TEX_CONTROLS), &Menu::doControls},
				{Ressources::getInstance()->getText(TEX_RETURN), &Menu::doReturn}}},
		{MEN_GRA, {{Ressources::getInstance()->getText(TEX_AUDIO), &Menu::doAudio},
				{Ressources::getInstance()->getText(TEX_GRAPHICS), &Menu::doGraphics},
				{Ressources::getInstance()->getText(TEX_CONTROLS), &Menu::doControls},
				{Ressources::getInstance()->getText(TEX_RETURN), &Menu::doReturn}}},
		{MEN_SOUND, {{Ressources::getInstance()->getText(TEX_AUDIO), &Menu::doAudio},
				{Ressources::getInstance()->getText(TEX_GRAPHICS), &Menu::doGraphics},
				{Ressources::getInstance()->getText(TEX_CONTROLS), &Menu::doControls},
				{Ressources::getInstance()->getText(TEX_RETURN), &Menu::doReturn}}},
		{MEN_CON, {{Ressources::getInstance()->getText(TEX_AUDIO), &Menu::doAudio},
				{Ressources::getInstance()->getText(TEX_GRAPHICS), &Menu::doGraphics},
				{Ressources::getInstance()->getText(TEX_CONTROLS), &Menu::doControls},
				{Ressources::getInstance()->getText(TEX_RETURN), &Menu::doReturn}}}
	}}
{
    Ressources   *ressources = Ressources::getInstance();

    _menu = MEN_MAIN;
    _music.openFromFile("resources/Darkened_Soul_Echoes_from_a_Dark_Past.ogg");
    _music.setVolume(100);
    _music.setLoop(true);
    _music.play();
	_view = ressources->getWindow()->getDefaultView();
}

Menu::~Menu()
{
}

void                Menu::doLaunch()
{
    Ressources   *ressources = Ressources::getInstance();

    ressources->setStatut(STA_GAME);
    std::cout << "launch" << std::endl;
}

void                Menu::doEditor()
{
    Ressources   *ressources = Ressources::getInstance();

    ressources->setStatut(STA_EDITOR);
    std::cout << "editor" << std::endl;
}

void                Menu::doResume()
{
    std::cout << "resume" << std::endl;
}

void                Menu::doOptions()
{
    std::cout << "option" << std::endl;
    _menu = MEN_OPT;
}

void                Menu::doExit()
{
    Ressources          *ressources = Ressources::getInstance();
    sf::RenderWindow    *window;

    window = ressources->getWindow();
    std::cout << "exit" << std::endl;
    window->close();
}

void                Menu::doAudio()
{
    std::cout << "audio" << std::endl;
    _menu = MEN_SOUND;
}

void                Menu::doGraphics()
{
    std::cout << "graphics" << std::endl;
    _menu = MEN_GRA;
}

void                Menu::doControls()
{
    std::cout << "controls" << std::endl;
    _menu = MEN_CON;
}

void                Menu::doReturn()
{
    std::cout << "return" << std::endl;
    _menu = MEN_MAIN;
}

void                Menu::doVolume()
{
    std::cout << "return" << std::endl;
    if (_music.getVolume() != 0)
        _music.setVolume(_music.getVolume() - 10);
}

void                Menu::doSound()
{
    std::cout << "return" << std::endl;
    if (_music.getStatus() == sf::Music::Stopped)
        _music.play();
    else if (_music.getStatus() == sf::Music::Playing)
        _music.stop();
}

void    Menu::event()
{
    Ressources          *ressources = Ressources::getInstance();
    sf::RenderWindow    *window;
    sf::Event           event;

    window = ressources->getWindow();
    sf::Vector2f v1=static_cast<sf::Vector2f>(sf::Mouse::getPosition(*window));
    while (window->pollEvent(event))
    {
        if (event.type == sf::Event::Closed
            || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            window->close();
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            for (auto it = _buttons[_menu].begin(); it != _buttons[_menu].end(); ++it)
                if (it->isContains(v1))
				{
                    (this->*(it->ptr))();
					break ;
				}
        }
    }
}

void     Menu::main()
{
    Ressources          *ressources = Ressources::getInstance();
    sf::RenderWindow    *window;

    event();

    window = ressources->getWindow();
	window->setView(_view);
	window->setMouseCursorVisible(true);
    window->clear(sf::Color::Black);

    if (ressources->getStatut() == STA_GAME)
        return;
    else
	{
		int total = 0;
		for (auto it=_buttons[_menu].begin(); it != _buttons[_menu].end(); ++it)
			total += it->height();
		sf::View view = window->getView();
		int pos = view.getSize().y / 2 - total / 2;
        for (auto it=_buttons[_menu].begin(); it != _buttons[_menu].end(); ++it)
        {
            it->update(pos);
			pos += it->height();
            window->draw(*it);
        }
	}

    window->display();
}
