/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Control.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/07/14 15:09:48 by lcaminon          #+#    #+#             */
//   Updated: 2014/08/11 02:52:26 by lcaminon         ###   ########.fr       //
/*                                                                            */
/* ************************************************************************** */

#include <Control.h>

Control::Control() : _gride(sf::Lines, (128 + 1) * 4)
{
	_backGround.setFillColor(sf::Color::Black);
	_backGround.setOutlineThickness(1 / SCALE);
	_backGround.setOutlineColor(sf::Color::White);
	unsigned int size = _gride.getVertexCount() / 4;
	for (unsigned int i = 0; i < size; ++i)
	{
		_gride[i * 4].position = sf::Vector2f((-(size / 2 * Brick::brickSize) + i * Brick::brickSize) / SCALE, (-(size / 2 * Brick::brickSize)) / SCALE);
		_gride[i * 4].color = sf::Color::Red;
		_gride[i * 4 + 1].position = sf::Vector2f((-(size / 2 * Brick::brickSize) + i * Brick::brickSize) / SCALE, (-(size / 2 * Brick::brickSize) + (size - 1) * Brick::brickSize) / SCALE);
		_gride[i * 4 + 1].color = sf::Color::Red;
		_gride[i * 4 + 2].position = sf::Vector2f((-(size / 2 * Brick::brickSize)) / SCALE, (-(size / 2 * Brick::brickSize) + i * Brick::brickSize) / SCALE);
		_gride[i * 4 + 2].color = sf::Color::Red;
		_gride[i * 4 + 3].position = sf::Vector2f((-(size / 2 * Brick::brickSize) + (size - 1) * Brick::brickSize) / SCALE, (-(size / 2 * Brick::brickSize) + i * Brick::brickSize) / SCALE);
		_gride[i * 4 + 3].color = sf::Color::Red;
	}
	_principalLists["luminosity"] = {{"jour"}, {"crepuscule"}, {"nuit"}};
	_principalLists["meteo"] = {{"soleil"}, {"nuage"}, {"orage"}};
	for (auto &list : _principalLists)
		_principalButtons.emplace(list.first, list.second);
}

void Control::update()
{
	for (auto &button : _principalButtons)
		button.second.update();
}

void Control::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(_gride, states);
	states.transform *= getTransform();
	target.draw(_backGround, states);
	for (auto &button : _principalButtons)
		target.draw(button.second, states);
}

void Control::setSize(sf::Vector2f size)
{
	_backGround.setSize(size);
	float increment = 60.f / SCALE;
	float y = increment;
	
	for (auto &button : _principalButtons)
	{
		button.second.setPosition(_backGround.getSize().x / 2, y);
		y += increment;
	}
}

bool Control::event(sf::Vector2f pos)
{
	pos = getTransform().getInverse().transformPoint(pos);
	for (auto &button : _principalButtons)
		if (button.second.event(pos))
			return (true);
	return (false);
}

sf::Color Control::getAmbiant()
{
	sf::Color luminosity = sf::Color::White;
	sf::Color color = sf::Color::White;

	if (*(_principalLists["meteo"].begin()) == "soleil")
		color = sf::Color(255, 236, 122, 255);
	else if (*(_principalLists["meteo"].begin()) == "nuage")
		color = sf::Color(193, 193, 193, 255);
	else if (*(_principalLists["meteo"].begin()) == "orage")
		color = sf::Color(60, 60, 60, 255);
	if (*(_principalLists["luminosity"].begin()) == "jour")
		luminosity = sf::Color(255, 255, 255, 255);
	else if (*(_principalLists["luminosity"].begin()) == "crepuscule")
		luminosity = sf::Color(60, 60, 60, 255);
	else if (*(_principalLists["luminosity"].begin()) == "nuit")
		luminosity = sf::Color::Black;
	return (color * luminosity);
}
