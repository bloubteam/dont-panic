/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Game.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/20 22:11:34 by lcaminon          #+#    #+#             */
/*   Updated: 2014/07/19 20:17:19 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAME_H
#define GAME_H

#include <common.h>
#include <IElement.h>
#include <Player.h>
#include <SoftCircle.h>
#include <Batitest.h>

class Game
{
	sf::RenderWindow *_window;
	sf::Shader _visionShader;
	b2Vec2 _gravity;
	b2World _world;
	sf::View _view;

	sf::RenderTexture _scene;
	sf::RenderTexture _light;
	sf::RenderTexture _vision;

	std::list<std::shared_ptr<IElement>> _elements;
	std::shared_ptr<Player> _player;

	void event();
	void prepareToMove();
	void luminosity();
	void vision();
	void draw();

public:
	Game();
	~Game();
	void main();
};

#endif
