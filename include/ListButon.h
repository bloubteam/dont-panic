/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ListButon.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/07/14 15:08:47 by lcaminon          #+#    #+#             */
/*   Updated: 2014/08/10 00:54:59 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LISTBUTON
#define LISTBUTON

#include <common.h>

class ListButon : public sf::Drawable, public sf::Transformable
{
	sf::CircleShape _left;
	sf::CircleShape _right;
	sf::Text _text;
	std::list<std::string> &_list;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

public:
	ListButon(std::list<std::string> &list);
	bool event(sf::Vector2f pos);
	void update();
};


#endif
