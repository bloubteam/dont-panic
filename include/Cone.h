/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cone.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/22 16:25:28 by lcaminon          #+#    #+#             */
/*   Updated: 2014/06/22 16:51:53 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONE
#define CONE

#include <common.h>
#include <RayCastVision.h>

class Cone : public sf::Drawable
{
	sf::VertexArray _visionArray;
	sf::Color _in;
	sf::Color _out;
	float _def;

public:
	Cone(sf::Color in, sf::Color out, int def = 100);
	void update(b2Vec2 origin, float dirAngle, float opAngle, float dist, b2World &world);
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif
