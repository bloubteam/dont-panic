/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Control.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/07/14 15:07:47 by lcaminon          #+#    #+#             */
/*   Updated: 2014/08/11 01:26:12 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTROL
#define CONTROL

#include <common.h>
#include <ListButon.h>
#include <Brick.h>

class Control : public sf::Drawable, public sf::Transformable
{
	sf::RectangleShape _backGround;
	std::map<std::string, std::list<std::string>> _principalLists;
	std::map<std::string, ListButon> _principalButtons;
	sf::VertexArray _gride;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

public:
	Control();
	void setSize(sf::Vector2f size);
	void update();
	bool event(sf::Vector2f pos);
	sf::Color getAmbiant();
};

#endif
