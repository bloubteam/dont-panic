/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Editor.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/07/12 17:20:58 by lcaminon          #+#    #+#             */
/*   Updated: 2014/07/20 22:03:10 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EDITOR_H
#define EDITOR_H

#include <common.h>
#include <IElement.h>

class Editor
{

	sf::RenderWindow _window;
	b2Vec2 _gravity;
	b2World _world;

	Control _control;

	sf::RenderTexture _light;
	std::list<std::shared_ptr<IElement>> _elements;

	void event();
	void luminosity();
	void draw();
	void update();

public:
	Editor();
	~Editor();
	void main();
};

#endif
