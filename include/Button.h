#ifndef BUTTON_H_INCLUDED
#define BUTTON_H_INCLUDED

#include    <SFML/Graphics.hpp>

class Menu;

class       Button : public sf::Drawable
{
    sf::Text            _button;

    public:
    Button(std::string name, void (Menu::*ptr)());
    bool                    isContains(const sf::Vector2f);
  	virtual void            draw(sf::RenderTarget& target, sf::RenderStates states) const;
    void                    (Menu::*ptr)();
    void                    update(int height);
	int						height();
};

#endif // BUTTON_H_INCLUDED
