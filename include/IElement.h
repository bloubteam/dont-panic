/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IElement.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/21 01:21:05 by lcaminon          #+#    #+#             */
/*   Updated: 2014/07/05 14:53:48 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IELEMENT
#define IELEMENT

#include <common.h>

class IElement : public sf::Drawable, public sf::Transformable
{
public:
	virtual bool dead() = 0;
	virtual void remove(b2World &world) = 0;
	virtual void update(sf::RenderTexture &luminosity, b2World &world, float timeStep) = 0;
	virtual void luminosity(sf::RenderTexture &luminosity, b2World &world) = 0;
};

#endif
