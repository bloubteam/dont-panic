/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Batitest.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/23 12:09:52 by lcaminon          #+#    #+#             */
/*   Updated: 2014/07/07 23:04:42 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BATITEST
#define BATITEST

#include <IElement.h>
#include <Brick.h>

class Batitest : public IElement
{
	std::list<std::shared_ptr<IElement>> elements; //element
	//lumières

public:
	b2Body *self;
	static std::shared_ptr<IElement> make(b2World &world);

	Batitest(b2World &world);
	bool dead();
	void remove(b2World& world);
	void update(sf::RenderTexture &luminosity, b2World &world, float timeStep);
	void luminosity(sf::RenderTexture &luminosity, b2World &world);
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif
