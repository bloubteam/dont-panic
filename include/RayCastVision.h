/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RayCastVision.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/22 14:10:11 by lcaminon          #+#    #+#             */
/*   Updated: 2014/06/22 15:25:45 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RAYCASTVISION
#define RAYCASTVISION

#include <common.h>

class RayCastVision : public b2RayCastCallback
{
	float _fraction;

public:
	RayCastVision();
	float32 ReportFixture(b2Fixture* fixture, const b2Vec2 &point,
						  const b2Vec2& normal, float32 fraction);
	float getFraction();
};

#endif
