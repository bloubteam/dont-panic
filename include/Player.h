/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Player.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/21 01:43:06 by lcaminon          #+#    #+#             */
/*   Updated: 2014/07/05 14:34:00 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLAYER
#define PLAYER

#include <common.h>
#include <IElement.h>
#include <Cone.h>

class Player : public IElement
{
	sf::RectangleShape _body;
	sf::RectangleShape _leftArm;

	Cone _coneVision;
	float _fishEye;
	float _distVision;
	bool _running;

public:
	b2Body *self;

	static std::shared_ptr<Player> make(b2World& world);

	Player(b2Body *oself);
	bool dead();
	void remove(b2World &world);
	void move(sf::RenderWindow &window);
	void vision(sf::RenderTexture &vision, b2World &world);
	void update(sf::RenderTexture &luminosity, b2World &world, float timeStep);
	void luminosity(sf::RenderTexture &luminosity, b2World &world);
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif
