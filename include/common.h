/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/20 22:23:23 by lcaminon          #+#    #+#             */
/*   Updated: 2014/07/14 15:05:48 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMON
#define COMMON

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <Box2D/Box2D.h>
#include <memory>
#include <array>
#include <list>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <Ressources.hh>

static const float SCALE = 30.f;
static const float DEGTORAD = 0.0174532925199432957f;
static const float RADTODEG = 57.295779513082320876f;

static const int COL_LIGHT = 1;
static const int COL_VISION = 2;
static const int COL_REFLECT = 4;
static const int COL_PHYSIC = 8;

#endif
