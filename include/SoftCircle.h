/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SoftCircle.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/21 17:14:52 by lcaminon          #+#    #+#             */
/*   Updated: 2014/06/22 16:33:07 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SOFTCIRCLE
#define SOFTCIRCLE

#include <common.h>

class		SoftCircle : public sf::Drawable, public sf::Transformable
{
	sf::VertexArray _inside;
	sf::VertexArray _outside;
	float		_plainRadius;
	float		_degradeSize;
	sf::Color _in;
	sf::Color _out;

	void calcPos();
public:
	SoftCircle(sf::Color in, sf::Color out, float plainRadius, float degradeSize);
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};


#endif
