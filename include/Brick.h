/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Batitest.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/23 12:09:52 by lcaminon          #+#    #+#             */
/*   Updated: 2014/07/07 23:05:40 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BRICK
#define BRICK

#include <IElement.h>

class Brick : public IElement
{
	sf::RectangleShape _brick;

public:
	static constexpr float brickSize = 30.f;

	b2Body *self;
	static std::shared_ptr<IElement> make(b2World &world, sf::Color type, float posx = 0, float posy = 0);

	Brick(b2Body *oself, sf::Color type);
	bool dead();
	void remove(b2World& world);
	void update(sf::RenderTexture &luminosity, b2World &world, float timeStep);
	void luminosity(sf::RenderTexture &luminosity, b2World &world);
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif
