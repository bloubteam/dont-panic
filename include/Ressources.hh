//
// Ressources.hh for arbrePhilo in /home/lcaminon/Documents/projets/arbrePhilo
//
// Made by Loic Caminondo
// Email   <loic@caminondo.fr>
//
// Started on  Thu Sep 12 09:16:11 2013 Loic Caminondo
// Last update Sat Nov 16 17:38:01 2013 Loic Caminondo
//

#ifndef				RESSOURCES_HH
#define				RESSOURCES_HH

#include			<SFML/Graphics.hpp>
#include			<vector>
#include			<string>
#include			<Singleton.hh>

enum				State
{
	STA_MENU,
	STA_GAME,
	STA_EDITOR
};

enum				Quality
  {
    QUA_LOW,
    QUA_MED,
    QUA_HIG
  };

enum				FontType
  {
    FON_DEFAULT
  };

enum				TextureType
  {
  };

enum				Language
  {
    LAN_FR,
    LAN_EN,
    LAN_EO
  };

enum				Text
  {
    TEX_NEW,
    TEX_CONTINUE,
	TEX_EDITOR,
    TEX_AGAIN,
    TEX_OPTIONS,
    TEX_AUDIO,
    TEX_GRAPHICS,
    TEX_CONTROLS,
    TEX_RETURN,
    TEX_SUCCESS,
    TEX_ANALYSE,
    TEX_EXIT,
    TEX_STATS,
    TEX_SKILLS,
    TEX_YES,
    TEX_NO,
    TEX_AREYOUSURE,
    TEX_COMINGSOON
  };

class				Ressources : public Singleton<Ressources>
{
  sf::Font			_font;
  std::map<TextureType, sf::Texture*>	_texture;
  std::map<Language, std::map<Text, std::string> > _texts;
  sf::RenderWindow		*_window;
  Quality			_quality;
  Language			_language;
  sf::Thread			_th_loading;
  bool  			_isLoading;
  bool				_loadingError;
  State               _statut;

  void loading();
public:
  Ressources();
  ~Ressources();
  void load(sf::RenderWindow *window);
  bool isLoading();
  bool isOk();
  sf::Font *getFont(FontType type = FON_DEFAULT);
  sf::Texture *getTexture(TextureType type);
  sf::RenderWindow *getWindow();
  std::string getText(Text text);
  int getDef();
  State getStatut();
  void setStatut(State statut);
};

#endif
