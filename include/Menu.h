#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include        <Button.h>
#include        <common.h>

enum            WhichMenu
{
    MEN_MAIN,
    MEN_OPT,
    MEN_GRA,
    MEN_SOUND,
    MEN_CON
};

class Menu
{
	void                event();
    void                doLaunch();
    void                doEditor();
    void                doResume();
    void                doOptions();
    void                doExit();
    void                doAudio();
    void                doGraphics();
    void                doControls();
    void                doReturn();
    void                doVolume();
    void                doSound();
    WhichMenu           _menu;
    sf::Music           _music;
	std::map<WhichMenu, std::list<Button>> _buttons;
	sf::View			_view;

public:
    Menu();
	~Menu();
	void main();
};

#endif // MENU_H_INCLUDED
