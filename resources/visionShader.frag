uniform vec2 size;
uniform sampler2D texture;
uniform vec2 view;

vec4 pixelate(float pixel_threshold)
{
    float factor = 1.0 / (pixel_threshold / 30. + 0.001);
	vec2 pos = floor(gl_TexCoord[0].xy * factor + 0.5) / factor;
	return (texture2D(texture, pos) * gl_Color);
}

vec4 epileptic_killer(float pixel_threshold)
{
    float factor = 1.0 / (pixel_threshold + 0.001);
	factor = 1.;
	vec2 pos = floor(gl_TexCoord[0].xy * factor + 0.5) / factor;
	pos = gl_TexCoord[0].xy + vec2(1, 1);
	vec2 max = pos + (gl_TexCoord[0].xy - pos) * 2.;
	vec4 color;
	int i = 0;
	while (pos.x <= max.x)
	{
		while (pos.y <= max.y)
		{
			color += texture2D(texture, pos);
			++i;
			++pos.y;
		}
		++pos.x;
	}
	color.a /= float(i);
	color.r /= float(i);
	color.g /= float(i);
	color.b /= float(i);
	return (color * gl_Color);
}

vec4 floute(float pixel_threshold)
{
    float factor = floor(pixel_threshold * pixel_threshold * 50.);
	if (factor > 10.)
	   factor = 10.;
	vec2 increment = vec2(1, 1) / size;
	vec2 max = gl_TexCoord[0].xy + increment * factor;
	float divisor = factor * factor;
	vec4 color = vec4(0,0,0,0);
	vec2 pos = gl_TexCoord[0].xy - increment * factor;
	float savey = pos.y;
	while (pos.x <= max.x)
	{
		pos.y = savey;
		while (pos.y <= max.y)
		{
			color += texture2D(texture, pos) / divisor;
			pos.y += increment.y;
		}
		pos.x += increment.x;
	}
	return (color * gl_Color);
}

void main()
{
	vec2 dview = normalize(view - size / 2.);
	vec2 dpixel = normalize(gl_TexCoord[0].xy * size - size / 2.);
	float angle = acos(dot(dview, dpixel));
	float pixel_threshold = 0.;
	if (degrees(angle) < 105.)
	{
		angle = angle - radians(90) / 2.;
		pixel_threshold = angle;
		if (pixel_threshold < 0.)
	   	   pixel_threshold = 0.;
	}
	gl_FragColor = pixelate(pixel_threshold);
}
